The work presents solutions for recognizing motor movements based on biological signals,
such as electroencephalography (EEG) and electromyography (EMG). Two public datasets were
used that contained one of these signals. The EEG dataset contained recordings of patients
performing motor movements, obtained from the patients’ scalps. Two pipelines have been
constructed to classify motor and imaginary tasks based on the EEG recordings. One based on
common spatial patterns (CSP) algorithm and Linear Discriminant Analysis (LDA) or support
vector machine (SVM) classifier and one based on a convolutional neural network (CNN). The
EMG dataset contained recordings of 34 different hand gestures performed by patients, obtained
from the patient’s forearm using HD-sEMG sensors. Two different preprocessing methods of
the signal have been tested on the same deep learning model for gesture recognition. Both
methods were based on creating images from samples of the EMG signal. All methods achieved
satisfactory results, proving the possibility of motion detection from EEG and EMG biological
signals.
