import numpy as np
import tensorflow as tf
from collections import deque
from utils.preprocessing import EEGDataGenerator
from utils.models import ConvNet
from utils.model_evaluation import ModelEvaluator
from sklearn.model_selection import train_test_split, StratifiedKFold
from keras import losses
from keras.constraints import max_norm
from keras.callbacks import ModelCheckpoint
from keras.layers import Conv2D, AveragePooling2D, BatchNormalization
from keras.layers import Flatten, Dense, Dropout, Activation
from keras.layers import DepthwiseConv2D, SeparableConv2D


def main(subject: int):
    print(f"-------------------- SUBJECT: {subject}--------------------")
    # motor imaginary: hands vs  feet
    runs = [5, 6, 9, 10, 13, 14]
    event_ids = dict(T1=2, T2=3)
    t_min, t_max = 0., 4.5
    filter_low, filter_high = 7., 30.

    # paths
    path = r'E:\Program\pythom\datasets'
    results_path = rf'../storage/results/EEG/Subject_{subject}'

    eeg_gen = EEGDataGenerator(path=path, subjects=subject, runs=runs)
    X, labels = eeg_gen.return_preprocessed_data(event_ids=event_ids, t_min=t_min, t_max=t_max,
                                                 filter_low=filter_low, filter_high=filter_high)
    X_train, X_test, y_train, y_test = train_test_split(X, labels, train_size=0.75, random_state=52)

    channels, samples = 64, int(X.shape[2])
    input_shape = (channels, samples, 1)
    learning_rate = 1e-4
    batch_size = 16
    epochs = 200
    validation_split = 0.2
    optimizer = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    loss = losses.binary_crossentropy
    checkpointer = ModelCheckpoint(filepath=rf'../storage/models/checkpoints/EEGNet_subject{subject}_checkpointer.h5',
                                   save_best_only=True)
    metrics = ['accuracy', 'AUC']

    # Keras Implementation of EEGNet: http://iopscience.iop.org/article/10.1088/1741-2552/aace8c/meta
    F1, F2, D = 8, 16, 2
    kernLength = 40
    norm_rate = 0.25
    nb_classes = len(np.unique(labels))
    skf = StratifiedKFold(n_splits=3)

    EEGNet = ConvNet(X_train, y_train)
    EEGNet.create_input_layer(shape=input_shape)
    EEGNet.add_layer(Conv2D(F1, (1, kernLength), padding='same',
                            input_shape=(channels, samples, 1),
                            use_bias=False))
    EEGNet.add_layer(BatchNormalization())
    EEGNet.add_layer(DepthwiseConv2D((channels, 1), use_bias=False,
                                     depth_multiplier=D,
                                     depthwise_constraint=max_norm(1.)))
    EEGNet.add_layer(BatchNormalization())
    EEGNet.add_layer(Activation('elu'))
    EEGNet.add_layer(AveragePooling2D((1, 10)))
    EEGNet.add_layer(Dropout(0.5))
    EEGNet.add_layer(SeparableConv2D(F2, (1, samples),
                                     use_bias=False,
                                     padding='same'))
    EEGNet.add_layer(BatchNormalization())
    EEGNet.add_layer(Activation('elu'))
    EEGNet.add_layer(AveragePooling2D((1, 5)))
    EEGNet.add_layer(Dropout(0.5))
    EEGNet.add_layer(Flatten(name='flatten'))
    EEGNet.add_layer(Dense(nb_classes, name='dense',
                           kernel_constraint=max_norm(norm_rate)))
    EEGNet.add_layer(Activation('sigmoid', name='sigmoid'))

    # compiling the model and executing training
    EEGNet.compile_model(optimizer=optimizer, loss=loss, metrics=metrics)
    EEGNet.set_callbacks(callback=checkpointer)
    EEGNet.print_model()
    history = EEGNet.train(epochs=epochs, batch_size=batch_size,
                           validation_split=validation_split, cv=skf)

    EEGNet.load_weights(checkpointer.filepath)
    EEGNet.save_model(rf'../storage/models/EEGNet')

    # evaluation of the model
    predictions = EEGNet.predict(X_test)
    true_labels = y_test
    evaluator = ModelEvaluator(true_labels=true_labels, predicted_labels=predictions,
                               history=history)

    evaluator.plot_metrics(save_path=results_path)
    evaluator.print_classification_report()
    evaluator.plot_confusion_matrix(save_path=results_path)
    evaluator.save_results_to_csv(path=r'../storage/results/EEG/EEGNet_Results.csv', data={"Subject": subject})


if __name__ == '__main__':
    tf.config.set_visible_devices([], "GPU")
    deque(map(main, np.arange(1, 11)))
