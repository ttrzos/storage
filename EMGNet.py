import numpy as np
import tensorflow as tf
from collections import deque
from utils.preprocessing import EMGDataGenerator
from utils.model_evaluation import ModelEvaluator
from utils.models import ConvNet
from sklearn.model_selection import train_test_split
from keras import losses
from keras.callbacks import ModelCheckpoint
from keras.layers import Conv2D, AveragePooling2D, Flatten, Dense


def main(subject: int):
    print(f"-------------------- SUBJECT: {subject}--------------------")
    method = 'sensor-wise'
    window_size, overlap = 256, 128
    image_shape = (16, 16)

    # paths
    path = r"E:\Program\pythom\datasets\MNE-eegbci-data\files\hd-semg\1.0.0\pr_dataset"
    results_path = rf'../storage/results/EMG/Subject_{subject}'

    emg_gen = EMGDataGenerator(path=path, subject=subject)
    X_raw, labels_raw = emg_gen.return_raw_data()
    trials, samples, sensors = X_raw.shape[0], X_raw.shape[1], X_raw.shape[2]
    nb_classes = len(np.unique(labels_raw))
    input_shape = (sensors, image_shape[0], image_shape[1])

    X, labels = emg_gen.return_preprocessed_data(method=method, window_size=window_size,
                                                 overlap=overlap,
                                                 image_shape=image_shape)

    X_train, X_test, y_train, y_test = train_test_split(X, labels, train_size=0.8, random_state=42, stratify=labels)

    batch_size = 256
    epochs = 50
    validation_split = 0.2

    lr_schedule = tf.optimizers.schedules.ExponentialDecay(
        initial_learning_rate=5e-4,
        decay_steps=10000,
        decay_rate=0.96,
        staircase=True)
    optimizer = tf.keras.optimizers.Adam(learning_rate=lr_schedule)
    loss = losses.categorical_crossentropy
    checkpointer = ModelCheckpoint(filepath=rf'../storage/models/checkpoints/EMGNet_subject{subject}_checkpointer.h5',
                                   save_best_only=True)
    metrics = ['accuracy', 'AUC']

    EMGNet = ConvNet(X_train, y_train)
    EMGNet.create_input_layer(shape=input_shape)
    EMGNet.add_layer(Conv2D(filters=32, kernel_size=(3, 3), padding='same',
                            strides=(1, 1), activation='relu'))
    EMGNet.add_layer(AveragePooling2D((2, 2)))
    EMGNet.add_layer(Conv2D(filters=64, kernel_size=(3, 3), padding='same',
                            strides=(1, 1), activation='relu'))
    EMGNet.add_layer(AveragePooling2D((2, 2)))
    EMGNet.add_layer(Flatten(name='flatten'))
    EMGNet.add_layer(Dense(nb_classes, activation='softmax'))

    EMGNet.print_model()
    EMGNet.plot_model_architecture()

    EMGNet.compile_model(optimizer=optimizer, loss=loss, metrics=metrics)
    EMGNet.set_callbacks(callback=checkpointer)

    history = EMGNet.train(epochs=epochs, batch_size=batch_size, validation_split=validation_split)

    EMGNet.load_weights(checkpointer.filepath)
    EMGNet.save_model(rf'../storage/models/EMGNet')

    predictions = EMGNet.predict(X_test)
    true_labels = y_test
    evaluator = ModelEvaluator(true_labels, predictions, history)

    evaluator.plot_metrics(save_path=results_path)
    evaluator.print_classification_report()
    evaluator.plot_confusion_matrix(save_path=results_path)
    evaluator.save_results_to_csv(path=r'../storage/results/EMG/EMGNet_Results.csv', data={"Subject": subject})


if __name__ == "__main__":
    tf.config.set_visible_devices([], "GPU")
    deque(map(main, np.arange(1, 11)))
