import numpy as np
import tensorflow as tf
import keras
import mne
from keras.models import Sequential, load_model
from keras.layers import Dense
from keras.layers import Input
from keras.callbacks import ModelCheckpoint
from sklearn.pipeline import Pipeline
from sklearn.model_selection import cross_val_score
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.svm import SVC
from mne.decoding import CSP
import matplotlib.pyplot as plt
from abc import ABC, abstractmethod


class AbstractModel(ABC):
    """
    Abstract model class
    """

    @abstractmethod
    def save_model(self, path):
        pass

    @abstractmethod
    def load_model(self, path):
        pass

    @abstractmethod
    def load_weights(self, path):
        pass

    @abstractmethod
    def compile_model(self, *args, **kwargs):
        pass

    @abstractmethod
    def add_layer(self, *args, **kwargs):
        pass

    @abstractmethod
    def print_model(self, *args):
        pass

    @abstractmethod
    def plot_model_architecture(self, *args):
        pass

    @abstractmethod
    def train(self, *args, **kwargs):
        pass

    @abstractmethod
    def predict(self, *args, **kwargs):
        pass


class ConvNet(AbstractModel):

    def __init__(self, x_train: np.array, y_train: np.array):
        """
        Constructor initializing the empty model, and collecting features and labels data intended for training
        :param x_train: np.array, features to feed the model
        :param y_train: np.array, labels to feed the model
        """
        self.model = Sequential()
        self.x_train = x_train
        self.y_train = y_train
        self.callbacks = []

    def save_model(self, path: str) -> None:
        """
        Function saving model under given path
        :param path: str, path to place where the model is to be saved
        """
        self.model.save(path)

    def load_model(self, path) -> None:
        """
        Function loading model from given path
        :param path: str, path to saved model
        """
        self.model = load_model(path)

    def load_weights(self, path):
        """
        Function loading saved weighs to the model
        :param path: str, path to the saved weights
        """
        self.model.load_weights(path)

    def print_model(self) -> None:
        """
        Function printing the summary of the sequential model
        """
        print(self.model.summary())

    def plot_model_architecture(self, *args):
        pass

    def create_input_layer(self, shape: tuple) -> None:
        """
        Function adding input layer to the sequential model
        :param shape: tuple, shape of the input data
        """
        self.model.add(Input(shape=shape))

    def add_layer(self, layer: keras.layers, *args, **kwargs) -> None:
        """
        Function adding layer to sequential model
        :param layer: layer object from tensorflow.keras.layers
        :param kwargs:
                    params: dict, parameters that layer object can inherit from
        """
        if 'params' in kwargs:
            self.model.add(layer(**kwargs['params']))
        else:
            self.model.add(layer)

    def set_callbacks(self, callback):
        """
        Function setting callbacks for training of the model
        :param callback:
        """
        if isinstance(callback, list):
            self.callbacks = callback
        else:
            self.callbacks = [callback]

    def compile_model(self, optimizer: tf.keras.optimizers, loss: tf.keras.losses, *args, **kwargs):
        """
        Function to set optimizer, loss function and additional parameters of the function
        :param optimizer:
        :param loss: loss function
        :param kwargs:
        """
        for key, value in kwargs.items():
            if key == 'metrics':
                self.model.compile(optimizer=optimizer, loss=loss, metrics=value)
            else:
                self.model.compile(optimizer=optimizer, loss=loss, metrics=['accuracy'])

    def train(self, epochs: int, batch_size: int,
              validation_split: float = 0.2, *args, **kwargs) -> tf.keras.callbacks.History:
        """
        Function used to train the sequential model on the data provided to the constructor of the class
        :param epochs: int, number of epochs to perform during training.
                       If cv parameter is provided epochs apply to every fold
        :param batch_size: int, size of batch
        :param validation_split: float, percent of training data intended for validation
        :param kwargs:
                    cv:
        :return history: tf.keras.callbacks.History, history of the training
        """
        for key, value in kwargs.items():
            if key == 'cv':
                history = {}
                for n_fold, (train_idx, val_idx) in enumerate(value.split(self.x_train, self.y_train)):
                    print(f"\n-----------Fold: {n_fold+1}-----------\n")
                    train_data, train_labels = self.x_train[train_idx], tf.keras.utils.to_categorical(self.y_train[train_idx])
                    val_data, val_labels = self.x_train[val_idx], tf.keras.utils.to_categorical(self.y_train[val_idx])
                    hist = self.model.fit(train_data, train_labels, epochs=epochs, batch_size=batch_size,
                                          validation_data=(val_data, val_labels),
                                          callbacks=self.callbacks)
                    if not history:
                        history = hist
                    else:
                        for k in hist.history.keys():
                            history.history[k].extend(hist.history[k])

                    for callback in self.callbacks:
                        if isinstance(callback, keras.callbacks.ModelCheckpoint):
                            print("---Loading previous fold best iteration weights---")
                            self.model.load_weights(callback.filepath)
                        else:
                            # future implementation
                            pass
                break
            else:
                raise KeyError("Provided parameters are not yet implemented")
        else:
            history = self.model.fit(self.x_train, tf.keras.utils.to_categorical(self.y_train),
                                     epochs=epochs, batch_size=batch_size,
                                     validation_split=validation_split,
                                     callbacks=self.callbacks)
        return history

    def predict(self, x_test: np.array, *args, **kwargs) -> np.array:
        """
        Function making prediction on provided data
        :param x_test: np.array, data on the basis of which model predicts its labels
        :return np.array:
        """
        predictions = self.model.predict(x_test)
        return predictions.argmax(axis=-1)


def csp_pipeline(epochs: mne.Epochs, labels: np.array, cv, n_components: int, classifier: str = 'LDA',
                 scoring: str = 'roc_auc', plot: bool = False, **kwargs):
    """
    Function creating pipeline with CSP algorithm and LDA/SVM classifier evaluating it's
    performance using cross validation an cross_val_score
    :param epochs: , epochs instance
    :param labels: np.array, labels for variables contained in epochs
    :param cv: , cross validation instance
    :param n_components: int, number of components for CSP algorithm
    :param classifier: str: {'LDA', SVM'}, type of classifier
    :param scoring: str, scoring metric
    :param plot: bool, if true CSP patterns plot will be shown
    :param kwargs:
                params: dict, model hyperparameters
                save_path: str, if provided, and 'plot' parameter is True, plot will be saved to provided path
    :
    """
    if classifier == 'LDA':
        model = LinearDiscriminantAnalysis()
    elif classifier == 'SVM':
        model = SVC()
    else:
        raise KeyError("Invalid classifier!")

    for key, value in kwargs.items():
        if key == 'params':
            model.set_params(**value)

    epochs_data = epochs.get_data()
    # Classifier
    csp = CSP(n_components, reg=None, log=True, norm_trace=False)
    clf = Pipeline([('CSP', csp), (classifier, model)])
    scores = cross_val_score(clf, epochs_data, labels, scoring=scoring,
                             cv=cv, n_jobs=1)
    if plot:
        csp.fit_transform(epochs_data, labels)
        csp.plot_patterns(epochs.info, ch_type='eeg', units='Patterns (AU)', size=1.5)
        if 'save_path' in kwargs:
            plt.savefig(kwargs['save_path'])
        plt.show()
    acc = np.mean(scores)
    print(f"Classification accuracy: {acc}")
    return scores, acc



