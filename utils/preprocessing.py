import numpy as np
import os
import re
import mne
import wfdb
from mne.datasets import eegbci
from typing import Union
from abc import abstractmethod, ABC


class AbstractGenerator(ABC):

    @abstractmethod
    def extract_raw_data(self, *args, **kwargs):
        pass

    @abstractmethod
    def return_raw_data(self):
        pass

    def return_preprocessed_data(self, *args, **kwargs):
        pass


class EEGDataGenerator(AbstractGenerator):
    """
    Class extracting and preprocessing EEG-BCI data located under provided path.
    Link to the data: https://physionet.org/content/eegmmidb/1.0.0/
    """

    def __init__(self, path: str, subjects: Union[int, np.array], runs: list):
        """
        Constructor extracting data from the provided path
        :param path: str, location path to the EEG-BCI data
        :param subjects: [int, np.array], chosen subjects to extract the data from
        :param runs: list, numbers of runs to extract
        """
        self.path = path
        self.runs = runs
        self.subjects = subjects
        self.raw = self.extract_raw_data()

    def extract_raw_data(self) -> mne.io.Raw:
        """
        Function extracting raw data from provided path to the constructor
        :return: raw data containing features and labels
        """
        if isinstance(self.subjects, int):
            raw_paths = eegbci.load_data(self.subjects, self.runs)
            raw = mne.io.concatenate_raws([mne.io.read_raw_edf(path, preload=True) for path in raw_paths])
        # elif isinstance(self.subjects, np.array):
        #     raw_paths = [eegbci.load_data(subject, self.runs) for subject in self.subjects]
        #     raw = mne.io.concatenate_raws([mne.io.read_raw_edf(path, preload=True) for path in np.concatenate(raw_paths)])
        else:
            raise TypeError("Incorrect type of variable 'subject'!")
        return raw

    def return_raw_data(self) -> mne.io.Raw:
        """
        Function returning the raw data
        :return: raw data
        """
        return self.raw

    def return_preprocessed_data(self, event_ids: dict, t_min: float = -1., t_max: float = 4.,
                                 filter_low: float = 7., filter_high: float = 30.,
                                 filter_design: str = 'firwin', return_epochs: bool = False) -> [np.array, np.array]:
        """
        Function preprocessing raw signal data between t_min and t_max using frequency filter
        :param event_ids: dict, ids of the events to extract
        :param t_min: float, start time of epochs in seconds
        :param t_max: float, end time of epochs in seconds
        :param filter_low:  float, lower pass-band frequency edge in HZ
        :param filter_high: float, upper pass-band frequency edge in HZ
        :param filter_design: str, name of the filter design: {'firwin', firwin2'}
        :param return_epochs: bool, if true function will return epochs object
        :return: preprocessed features and corresponding labels
        """
        eegbci.standardize(self.raw)
        montage = mne.channels.make_standard_montage('standard_1005')
        self.raw.set_montage(montage)
        self.raw.filter(filter_low, filter_high, fir_design=filter_design, skip_by_annotation='edge')
        events, _ = mne.events_from_annotations(self.raw, event_id=event_ids)
        picks = mne.pick_types(self.raw.info, meg=False, eeg=True, stim=False, eog=False,
                               exclude='bads')
        epochs = mne.Epochs(self.raw, events, event_ids, tmin=t_min, tmax=t_max, proj=False,
                            picks=picks, baseline=None, preload=True)
        if return_epochs:
            return epochs
        else:
            X = (epochs.get_data() * 1e6).astype(np.float64)
            labels = (epochs.events[:, -1] - 2).astype(np.int64)
            return X, labels

    @staticmethod
    def augment_noise_samples(samples: np.array, labels: np.array,
                              sample_number: int, mean: int = 0, std: float = 1.) -> [np.array, np.array]:
        """
        Function adding augmented samples using noise to the provided data
        :param samples: np.array, features
        :param labels: np.array, labels of the features
        :param sample_number: int, number of samples to augment
        :param mean: int: mean of the distribution
        :param std: float, standard deviation of the distribution
        :return: features with augmented samples and corresponding labels
        """
        noised_samples = np.random.normal(mean, std, (sample_number, samples.shape[1], samples.shape[2]))
        return np.concatenate((samples, noised_samples)), np.concatenate((labels, labels[:sample_number]))


class EMGDataGenerator(AbstractGenerator):
    """
    Class extracting and preprocessing sEMG data located under provided path.
    Link to the data: https://physionet.org/content/hd-semg/1.0.0/
    """
    
    def __init__(self, path: str, subject: int,
                 data_type: str = "dynamic_preprocess", label_type: str = "label_dynamic"):
        """
        Constructor extracting data from the provided path
        :param path: str, location path to the sEMG data
        :param subject: int, number of subject from which to extract the data
        :param data_type: str, type od data to extract from sEMG data: {'dynamic_preprocess', 'dynamic_raw', 'maintenance_preprocess', 'maintenance_raw'}
        :param label_type: str, type of labels to extract from subjects sEMG data: {'label_dynamic', 'label_maintenance'}
        """
        self.path = path
        self.subject_path = os.listdir(self.path)[::2][subject-1]
        self.data_type = data_type
        self.label_type = label_type
        self.X = self.extract_raw_data()
        self.labels = self.extract_labels()

    def extract_raw_data(self) -> np.array:
        """
        Function extracting raw features
        :return: features
        """
        data = []
        data_list = self.sorted_alphanumeric(os.listdir(rf"{self.path}\\{self.subject_path}"))
        if isinstance(self.subject_path, str):
            for sample in range(0, len(data_list), 2):
                if self.data_type in data_list[sample]:
                    data.append(wfdb.rdrecord(rf"{self.path}\\{self.subject_path}\\{data_list[sample].split('.')[0]}").p_signal)
        return np.array(data)

    def extract_labels(self) -> list:
        """
        Function extracting labels
        :return: labels
        """
        with open(rf"{self.path}\{self.subject_path}\{self.label_type}.txt") as f:
            lines = f.readlines()[0].split(",")
            f.close()
        return [int(line)-1 for line in lines]

    def return_raw_data(self):
        """
        Function returning the raw data
        :return: features, labels
        """
        return self.X, self.labels

    def return_preprocessed_data(self, method: str, window_size: int = 512, overlap: int = 256,
                                 image_shape: tuple = (32, 16)) -> [np.array, np.array]:
        """
        Function converting EMG signal data to sequence of images.
        Two techniques are available:  'sensor-wise' or 'sEMG-image'.
        :param method: str, method of preprocessing: {'sensor-wise', 'sEMG-image'}
        :param window_size: int, size of a single window to create images from
        :param overlap: int, number of overlapping samples in the window
        :param image_shape: tuple, shape of image to create from a window
        :return: features converted into images and corresponding labels
        """
        new_data = []
        trials, samples, sensors = self.X.shape
        reshaped_data = self.X.reshape(trials, sensors, samples)
        if method == 'sensor-wise':
            for trial in reshaped_data:
                for window in range(0, samples - overlap, overlap):
                    window_list = []
                    for sensor in trial:
                        window_list.append(sensor[window:window_size + window].reshape(image_shape))
                    new_data.append(window_list)
        elif method == 'sEMG-image':
            for trial in reshaped_data:
                for window in range(0, samples-overlap, overlap):
                    window_list = []
                    for sample in range(window, window_size+window, 1):
                        window_list.append(trial[:, sample].reshape(image_shape))
                    new_data.append(window_list)
        else:
            raise KeyError("Provided 'split' argument not supported!")
        return np.asarray(new_data), np.repeat(self.labels, samples / overlap - 1)

    @staticmethod
    def sorted_alphanumeric(data: list):
        """
        Function to sort in alphanumeric order
        :param data: list, list of items to sort
        :return: alphanumerically sorted array
        """
        convert = lambda text: int(text) if text.isdigit() else text.lower()
        alphanum_key = lambda key: [convert(c) for c in re.split('([0-9]+)', key)]
        return sorted(data, key=alphanum_key)
