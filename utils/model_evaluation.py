import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import tensorflow as tf
from sklearn.metrics import confusion_matrix, classification_report
import seaborn as sns


class ModelEvaluator:
    """
    Class evaluating the results of a machine learning model
    """

    def __init__(self, true_labels: np.array, predicted_labels: np.array,
                 history: tf.keras.callbacks.History, **kwargs):
        """
        Constructor collecting true and predicted labels, the model history and plot settings
        :param true_labels: np.array, true labels to comparison with predicted labels
        :param predicted_labels: np.array, labels previously predicted machine learning model
        :param history: tf.keras.callbacks.History, history of the trained model
        param kwargs:
                    style: str, seaborn style type
                    palette: str, seaborn color palette type
        """
        self.true_labels = true_labels
        self.predicted_labels = predicted_labels
        self.history = history
        sns.set_style(kwargs['style'] if 'style' in kwargs else 'darkgrid')
        sns.set_palette(kwargs['palette'] if 'palette' in kwargs else "colorblind")

    def plot_confusion_matrix(self, **kwargs):
        """
        Function used to visualize confusion matrix created based on true_labels and predicted_labels
        :param kwargs:
                    labels: list, names of the labels to show on the plot,
                    cmap: str, colors of the plotted confusion matrix,
                    linecolor: str, linecolor of the plotted confusion matrix,
                    linewidths: str, linewidth of the plotted confusion matrix},
        """
        cmap = kwargs.get('cmap', 'Blues')
        linecolor = kwargs.get('linecolor', 'white')
        linewidths = kwargs.get('linewidths', 1)
        save_path = kwargs.get('save_path', '')

        matrix = confusion_matrix(self.true_labels, self.predicted_labels)
        num_classes = matrix.shape[0]
        labels = kwargs['labels'] if 'labels' in kwargs else [str(i) for i in range(num_classes)]
        plt.figure(figsize=(num_classes, num_classes))
        sns.heatmap(matrix, cmap=cmap, linecolor=linecolor, linewidths=linewidths,
                    xticklabels=labels[0:num_classes], yticklabels=labels[0:num_classes],
                    annot=True, fmt='d')
        plt.title('Confusion Matrix')
        plt.ylabel('True Label')
        plt.xlabel('Predicted Label')
        if save_path:
            plt.savefig(save_path+'/ConfusionMatrix.png')
        plt.show()

    def print_classification_report(self):
        """
        Function printing classification report of true_labels and predicted_labels
        """
        print(classification_report(self.true_labels, self.predicted_labels))

    def plot_metrics(self, **kwargs):
        """
        Function plotting metrics from training of the model on training and validation set
        :param kwargs:
                    save_path: str, path to save the file to
        """
        history_keys = list(self.history.history.keys())
        history_pairs = [(x, history_keys[history_keys.index('val_'+x)])
                         for x in history_keys if 'val_'+x in history_keys]

        for metric, val in history_pairs:
            plt.plot(self.history.history[metric], label=metric)
            plt.plot(self.history.history[val], label=val)
            plt.title(f'Training {metric}')
            plt.xlabel('Epoch')
            plt.ylabel(metric)
            plt.legend()
            if 'save_path' in kwargs:
                plt.savefig(f"{kwargs['save_path']}/{metric}.png")
            plt.show()

    def save_results_to_csv(self, path: str, sep: str = ',', **kwargs):
        """
        Function appending to or creating new csv file to save metric scores from last iteration of training to provided
         path
        :param path: str, path to csv file where to append scores
        :param sep: str, separator of the csv file
        :param kwargs:
                    data: dict, additional data to save
        """
        training_metrics = dict(zip(self.history.history.keys(), [x[-1] for x in self.history.history.values()]))
        training_metrics['Test accuracy'] = np.mean(self.true_labels == self.predicted_labels)

        if 'data' in kwargs:
            training_metrics.update(kwargs['data'])
        training_metrics.update(self.history.params)
        return self.save_to_csv(training_metrics, path, sep)

    @staticmethod
    def save_to_csv(data: dict, path: str, sep: str = ','):
        try:
            results = pd.read_csv(path, sep=sep)
            results = results.append(data, ignore_index=True)
            results.to_csv(path, index=False)
            print('Appending results to csv file')
        except pd.errors.EmptyDataError:
            results = pd.DataFrame([data])
            results.to_csv(path)
            print("Saving results to csv file")
        return results
