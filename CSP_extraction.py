import numpy as np
from collections import deque
import matplotlib.pyplot as plt
from utils.preprocessing import EEGDataGenerator
from mne import Epochs, create_info, events_from_annotations
from mne.decoding import CSP
from mne.time_frequency import AverageTFR

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.model_selection import StratifiedKFold, cross_val_score
from sklearn.pipeline import Pipeline


def main(subject: int):
    print(f"-------------------- SUBJECT: {subject}--------------------")
    # motor imaginary: hands vs  feet
    runs = [5, 6, 9, 10, 13, 14]
    event_id = dict(hands=2, feet=3)

    # paths
    path = r'E:\Program\pythom\datasets'
    results_path = rf'../storage/results/EEG/Subject_{subject}'

    # Read data
    eeg_gen = EEGDataGenerator(path=path, subject=subject, runs=runs)
    raw = eeg_gen.return_raw_data()

    # Extract information form the raw file
    sfreq = raw.info['sfreq']
    events, _ = events_from_annotations(raw, event_id=dict(T1=2, T2=3))
    raw.pick_types(meg=False, eeg=True, stim=False, eog=False, exclude='bads')
    raw.load_data()

    # Assemble the classifier
    n_splits = 5

    lda = LinearDiscriminantAnalysis()
    csp = CSP(n_components=n_splits, reg=None, log=True, norm_trace=False)
    clf = Pipeline([('CSP', csp), ('LDA', lda)])

    cv = StratifiedKFold(n_splits=n_splits, shuffle=True, random_state=52)

    # Classification & time frequency parameters
    tmin, tmax = -.200, 4.
    n_cycles = 10.
    min_freq = 5.
    max_freq = 30.
    n_freqs = 8

    # Assemble list of frequency range tuples
    freqs = np.linspace(min_freq, max_freq, n_freqs)
    freq_ranges = list(zip(freqs[:-1], freqs[1:]))

    # Infer window spacing form the max freq and number of cycles to avoid gaps
    window_spacing = (n_cycles / np.max(freqs) / 2.)
    centered_w_times = np.arange(tmin, tmax, window_spacing)[1:]
    n_windows = len(centered_w_times)

    # init scores
    tf_scores = np.zeros((n_freqs - 1, n_windows))

    # Loop through each frequency range of interest
    for freq, (fmin, fmax) in enumerate(freq_ranges):

        # Infer window size based on the frequency being used
        w_size = n_cycles / ((fmax + fmin) / 2.)  # in seconds

        # Apply band-pass filter to isolate the specified frequencies
        raw_filter = raw.copy().filter(fmin, fmax, n_jobs=1, fir_design='firwin',
                                       skip_by_annotation='edge')

        # Extract epochs from filtered data, padded by window size
        epochs = Epochs(raw_filter, events, event_id, tmin - w_size, tmax + w_size,
                        proj=False, baseline=None, preload=True)
        epochs.drop_bad()

        labels = epochs.events[:, -1] - 2

        # Roll covariance, csp and lda over time
        for t, w_time in enumerate(centered_w_times):

            # Center the min and max of the window
            w_tmin = w_time - w_size / 2.
            w_tmax = w_time + w_size / 2.

            # Crop data into time-window of interest
            X = epochs.copy().crop(w_tmin, w_tmax).get_data()

            # Save mean scores over folds for each frequency and time window
            tf_scores[freq, t] = np.mean(cross_val_score(estimator=clf, X=X, y=labels,
                                                         scoring='roc_auc', cv=cv,
                                                         n_jobs=1), axis=0)

    # Set up time frequency object
    av_tfr = AverageTFR(create_info(['freq'], sfreq), tf_scores[np.newaxis, :],
                        centered_w_times, freqs[1:], 1)

    chance = np.mean(labels) # set chance level to white in the plot
    av_tfr.plot([0], vmin=chance, title="Time-Frequency Decoding Scores", cmap=plt.cm.Reds, show=False)
    plt.savefig(results_path+'/Score_Matrix.png')
    plt.show()


if __name__ == '__main__':
    deque(map(main, np.arange(1, 11)))
